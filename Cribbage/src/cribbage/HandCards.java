package cribbage;

import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public interface HandCards {

    void addRule(Rule rule);

    void removeRule(Rule rule);

    void sendHandCards(Hand hand, Hand starter, Deck deck);

    void setPlayer(int player);

    void setScore(int score);

    int scoreHandCards();
}
