package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class showRuns implements Rule{

    Hand hand = new Hand(null);

    public showRuns(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
        this.hand.insert(starter, false);
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        for (int i=3; i<6; i++){
            Hand[] runs = hand.extractSequences(i);
            if (runs.length>0){
                for (Hand run : runs){
                    score += i;
                    log.writeLog(String.format("score,P%d,%d,%d,run%d,%s", playerId, currentScore + score, score, i, Cribbage.cribbage.canonical(run)));
                }
            }
        }
        return score;
    }
}
