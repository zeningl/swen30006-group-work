package cribbage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class LogGenerator {

    private static final LogGenerator log = new LogGenerator();
    private final Path file_name = Path.of("cribbage.log");

    private LogGenerator(){
        try {
            Files.deleteIfExists(file_name);
        }catch (IOException e){System.err.println(e);}
    }

    public static LogGenerator getLog(){
        return log;
    }

    public void writeLog(String str){
        String s = str + "\n";
        try {
            Files.write(file_name, s.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }catch (IOException e){System.err.println(e);}
    }
}
