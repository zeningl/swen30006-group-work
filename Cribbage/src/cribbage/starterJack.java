package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class starterJack implements Rule{

    Card starter;

    public starterJack(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.starter = starter.get(0);
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        if (((Cribbage.Rank) starter.getRank()).order == 11){
            score += 2;
        }
        return score;
    }
}
