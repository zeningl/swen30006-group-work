package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class playTotal implements Rule{

    Hand hand = new Hand(null);
    Hand starter;
    boolean go = false;
    final int fifteen = 15;
    final int thirtyone = 31;

    public playTotal(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
        this.starter = starter;
        setGo();
    }

    private void setGo(){
        if (starter.getNumberOfCards() == 1){
            go = true;
        }
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;

        int total = Cribbage.cribbage.total(hand);

        if (total == fifteen){
            score += 2;
            log.writeLog(String.format("score,P%d,%d,%d,fifteen", playerId, currentScore + score, score));
        } else if (total == thirtyone){
            score += 2;
            log.writeLog(String.format("score,P%d,%d,%d,thirtyone", playerId, currentScore + score, score));
        } else if (go){
            score += 1;
            log.writeLog(String.format("score,P%d,%d,%d,go", playerId, currentScore + score, score));
            go = false;
        }

        return score;
    }
}
