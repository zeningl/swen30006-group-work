package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class playPairs implements Rule{

    Hand hand;

    public playPairs(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        int len;

        while ((len = hand.getNumberOfCards()) > 0){
            Hand[] pairs;
            if (len == 4){
                pairs = hand.extractQuads();
                if (pairs.length != 0) {
                    score += 12;
                    break;
                }
            } else if (len == 3){
                    pairs = hand.extractTrips();
                    if (pairs.length != 0){
                        score += 6;
                        break;
                    }
                } else if (len == 2){
                    pairs = hand.extractPairs();
                    if (pairs.length != 0){
                        score += 2;
                        break;
                }
            }
            hand.removeFirst(false);
        }
        if (score != 0){
            log.writeLog(String.format("score,P%d,%d,%d,pair%d", playerId, currentScore + score, score, len));
        }

        return score;
    }

}
