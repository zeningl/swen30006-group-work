package cribbage;

import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public interface Rule {

    LogGenerator log = LogGenerator.getLog();

    void receiveHandCards(Hand hand, Hand starter, Deck deck);

    int calculateScore(int playerId, int currentScore);
}
