package cribbage;

import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

import java.util.ArrayList;

public class CribbageHandCards implements HandCards{

    private ArrayList<Rule> rules;

    int player;
    int score = 0;

    public CribbageHandCards(){
        rules = new ArrayList<Rule>();
    }

    @Override
    public void addRule(Rule rule){
        rules.add(rule);
    }

    @Override
    public void removeRule(Rule rule){
        rules.remove(rule);
    }

    @Override
    public void sendHandCards(Hand hand, Hand starter, Deck deck){
        rules.forEach((rule) -> rule.receiveHandCards(hand, starter, deck));
    }

    @Override
    public void setScore(int score){this.score = score;}

    @Override
    public void setPlayer(int player){
        this.player = player;
    }

    @Override
    public int scoreHandCards() {
        for(Rule r : rules){
            score += r.calculateScore(player, score);
        }
        return score;
    }
}
