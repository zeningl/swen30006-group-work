package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class playRuns implements Rule{

    Hand hand = new Hand(null);

    public playRuns(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
    }

    @Override
    public int calculateScore(int playerId, int currentScore){

        int score = 0;
        int len;

        while ((len = hand.getNumberOfCards()) > 0){
            Hand[] runs = hand.extractSequences(len);
            if (runs.length != 0){
                score += len;
                log.writeLog(String.format("score,P%d,%d,%d,run%d", playerId, currentScore + score, score, len));
                break;
            }
            hand.removeFirst(false);
        }
        return score;
    }
}
