package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

import java.util.ArrayList;

public class showFifteens implements Rule{

    Hand hand = new Hand(null);

    public showFifteens(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
        this.hand.insert(starter, false);
    }


    @Override
    public int calculateScore(int playerId, int currentScore){
        int score;
        ArrayList<Integer> cardValues = new ArrayList<>(); //An array list that contains all card values.
        //Add values to the array list.
        for (Card c: hand.getCardList()){
            cardValues.add(((Cribbage.Rank) c.getRank()).value);
        }
        //Choose N numbers from a list, the sum of them should be 15.
        score = search(cardValues, playerId, currentScore);
        return score;
    }

    private int search(ArrayList<Integer> cardValues, int playerId, int currentScore){
        int len = cardValues.size(), bit = 1 << len, score = 0, goal = 15;
        for(int i=1; i<bit; i++){
            int sum = 0;
            Hand fifteen = new Hand(hand.getFirst().getDeck());
            for (int j=0; j < len; j++){
                if((i & 1 << j) != 0){
                    fifteen.insert(hand.get(j).getCardNumber(), false);
                    sum += cardValues.get(j);
                }
            }
            if (sum == goal){
                log.writeLog(String.format("score,P%d,%d,%d,fifteen,%s", playerId, currentScore + 2, 2, Cribbage.cribbage.canonical(fifteen)));
                score += 2;
            }
        }
        return score;
    }
}
