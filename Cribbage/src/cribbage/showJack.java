package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class showJack implements Rule{

    Hand hand = new Hand(null);
    Card starter;


    public showJack(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }

        this.starter = starter.get(0);
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        for (Card c : hand.getCardList()){
            if (((Cribbage.Rank) c.getRank()).order == 11){
                if (c.getSuitId() == starter.getSuitId()){
                    score += 1;
                    log.writeLog(String.format("score,P%d,%d,%d,jack,%s", playerId, currentScore + score, score, Cribbage.cribbage.canonical(c)));
                }
            }
        }
        return score;
    }
}
