package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;


public class showPairs implements Rule{

    Hand hand = new Hand(null);

    public showPairs(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
        this.hand.insert(starter, false);
    }

    @Override
    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        Hand[] pairs = hand.extractPairs();
        for (Hand pair : pairs){
            score += 2;
            log.writeLog(String.format("score,P%d,%d,%d,pair2,%s", playerId, currentScore + score, score, Cribbage.cribbage.canonical(pair)));
        }

        Hand[] trips = hand.extractTrips();
        for (Hand trip : trips) {
            score += 6;
            log.writeLog(String.format("score,P%d,%d,%d,pair3,%s", playerId, currentScore + score, score, Cribbage.cribbage.canonical(trip)));
        }

        Hand[] quads = hand.extractQuads();
        for (Hand quad : quads) {
            score += 12;
            log.writeLog(String.format("score,P%d,%d,%d,pair4,%s", playerId, currentScore + score, score, Cribbage.cribbage.canonical(quad)));
        }
        return score;
    }
}
