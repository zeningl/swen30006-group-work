package cribbage;

import ch.aplu.jcardgame.Card;
import ch.aplu.jcardgame.Deck;
import ch.aplu.jcardgame.Hand;

public class showFlush implements Rule{

    Hand hand = new Hand(null);
    Card starter;

    public showFlush(){}

    @Override
    public void receiveHandCards(Hand hand, Hand starter, Deck deck){
        this.hand = new Hand(deck);
        for (Card c : hand.getCardList()){
            this.hand.insert(c.clone(), false);
        }
        this.starter = starter.get(0);
    }



    public int calculateScore(int playerId, int currentScore){
        int score = 0;
        for (Cribbage.Suit suit : Cribbage.Suit.values()){
            int nSuit = hand.getNumberOfCardsWithSuit(suit);
            if (nSuit == 4){
                score += 4;
                if (starter.getSuit() == suit){
                    score += 1;
                    break;
                }
                log.writeLog(String.format("score,P%d,%d,%d,flush%d,%s", playerId, currentScore + score, score, score, Cribbage.cribbage.canonical(hand)));
            }
        }
        return score;
    }
}
